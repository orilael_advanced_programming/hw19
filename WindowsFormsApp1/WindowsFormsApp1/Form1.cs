﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void insertBtn_Click(object sender, EventArgs e)
        {
            string username = userBox.Text;
            string password = passBox.Text;
            string user;
            bool found = false;
            string line;
            string fileName = System.IO.Path.GetFullPath(Directory.GetCurrentDirectory() + @"\Users.txt");
 
            System.IO.StreamReader file = new System.IO.StreamReader(fileName);
            while ((line = file.ReadLine()) != null)
            {
                string[] words = line.Split(',');
                if (username.Equals(words[0]) && password.Equals(words[1]))
                {
                    user = words[0];
                    found = true;
                   
                }
            }
            file.Close();

            if(!found)
            {
                failLabel.Text = "Wrong Password or Username";
            }
            else
            {
                failLabel.Text = "Success";
                this.Hide();
                Form2 f2 = new Form2(username);

                f2.FormClosed += new FormClosedEventHandler(form2_FormClosed);
                f2.ShowDialog();
        
            }
        }


    }
}
