﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        private string username;
        public Form2(string username)
        {
            this.username = username;
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            CultureInfo invC = CultureInfo.InvariantCulture ;
          
            string fileName = System.IO.Path.GetFullPath(Directory.GetCurrentDirectory() + @"\" + username + @"BD.txt");
            string line;
            string date;
           
            date = monthCalendar1.SelectionRange.Start.ToString(invC);  // MM/DD/YY 00:00:00
            date = date.Remove(date.Length - 9, 9);  //Remove the time
            if(date[0] == '0')
            {
                date = date.Remove(0, 1);   //Remove 0 from start of Month
                if (date[2] == '0')
                {
                    date = date.Remove(2, 1);  //Remove 0 from start of Day
                }
            }
            else
            {
                if (date[3] == '0')
                {
                    date = date.Remove(3, 1);   //Remove 0 from start of Day
                }
            }
            System.IO.StreamReader file = new System.IO.StreamReader(fileName);
            while ((line = file.ReadLine()) != null)
            {
                string[] words = line.Split(',');
                if(words[1].Equals(date))
                {
                    txtMsg.Text = "At the selected date: " + words[0] + " is celebrating";
                    break;
                }
                else
                {
                    txtMsg.Text = "At the selected date no one is celebrating";
                }
            }
            file.Close();

        }

        private void add_Click(object sender, EventArgs e)
        {
            CultureInfo invC = CultureInfo.InvariantCulture;
            string name = nameAdd.Text;
            bool isEnglish = true;
            int i;
            for(i=0;i<name.Length;i++)
            {
                if (!Char.IsLetter(name[i])) //if not english letter
                    isEnglish = false;          
            }


            if ((!name.Equals("")) && isEnglish) //not emptry or letter
            {
                err.Text = "";
                bool help = true;
                string line;
                string fileName = System.IO.Path.GetFullPath(Directory.GetCurrentDirectory() + @"\" + username + @"BD.txt");
                string date = dateTimePicker1.Value.ToString(invC);
                date = date.Remove(date.Length - 9, 9);  //Remove the time
                if (date[0] == '0')
                {
                    date = date.Remove(0, 1);   //Remove 0 from start of Month
                    if (date[2] == '0')
                    {
                        date = date.Remove(2, 1);  //Remove 0 from start of Day
                    }
                }
                else
                {
                    if (date[3] == '0')
                    {
                        date = date.Remove(3, 1);   //Remove 0 from start of Day
                    }
                }

                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                while ((line = file.ReadLine()) != null)
                {
                    string[] words = line.Split(',');
                    if (words[1].Equals(date))
                    {
                        err.Text = "Date is already Taken!";
                        help = false;
                        break;
                    }
                }
                file.Close();

                if (help)   //if date does not exist
                {
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        sw.WriteLine("");
                        sw.Write(name + "," + date);
                        sw.Close();
                    }
                }
            }
            else if (name.Equals(""))
            {
                err.Text = "Must enter a name";
            }
            else
            {
                err.Text = "Only English letters!";
            }
             //Reset
            nameAdd.Clear();
            txtMsg.Text = "";
            dateTimePicker1.Value = DateTime.Now;
        }
    }
}
